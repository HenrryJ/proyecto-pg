'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('persona', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      apellidos: {
        type: Sequelize.STRING
      },
      nombres: {
        type: Sequelize.STRING
      },
      nickname: {
        type: Sequelize.STRING
      },
      fecha_nac: {
        type: Sequelize.DATEONLY
      },
      edad: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('persona');
  }
};