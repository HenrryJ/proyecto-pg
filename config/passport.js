/**
 * Modulo para encriptar claves
 * @type Module bcrypt|Module bcrypt
 */
var bCrypt = require('bcrypt'); 
/**
 * Modelos de la base de datos
 * @type Module models|Module models
 */
var models = require('./../models');
var cuenta = models.cuenta;
var persona = models.persona;
var rol = models.rol;
var sequelize = require('sequelize');

module.exports = function (passport) {
    var Cuenta = cuenta;
    var Persona = persona;
    var Rol = rol;
    var LocalStrategy = require('passport-local').Strategy;

    /**
     * Pemite serializar los users
     */
    passport.serializeUser(function (cuenta, done) {
        done(null, cuenta.id);
    });

    /**
     * Permite deserilize el users
     */
    passport.deserializeUser(function (id, done) {
        Cuenta.findOne({where: {id: id}, include: [{model: Persona, include: [{model: Rol}]}]}).then(function (cuenta) {
            if (cuenta) {
                var userinfo = {
                    id: cuenta.persona.external_id,
                    nombre: cuenta.persona.apellidos + " " + cuenta.persona.nombres,
                    nick: cuenta.persona.nickname,
                    rol: cuenta.persona.rol.nombre,
                    estado: cuenta.estado
                };
                console.log(userinfo);
                done(null, userinfo);
            } else {
                done(cuenta.errors, null);
            }
        });
    });

    /**
     * Inicio de sesion
     */
    passport.use('local-signin', new LocalStrategy({
        usernameField: 'correo',
        passwordField: 'clave',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    }, function (req, email, password, done) {
        var Cuenta = cuenta;
        var isValidPassword = function (userpass, password) {
            return bCrypt.compareSync(password, userpass);
        };
        const Op = sequelize.Op;
        Cuenta.findOne({where: {[Op.and]: {correo: email, estado: true}}}).then(function (cuenta) {
            if (!cuenta) {
                return done(null, false, {message: req.flash('error', 'Cuenta no existe o fue dada de baja comuniquese con el administrador del sitio')});
            }
            if (!isValidPassword(cuenta.clave, password)) {
                return done(null, false, {message: req.flash('error', 'Clave incorrecta')});
            }
            var userinfo = cuenta.get();
            return done(null, userinfo);
        }).catch(function (err) {
            console.log("Error:", err);
            return done(null, false, {message: req.flash('error', 'Cuenta erronea')});
        });
    }
    ));

};

