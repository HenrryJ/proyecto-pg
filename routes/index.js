var express = require('express');
var router = express.Router();
var passport = require('passport');
//--------UTILIDADES----//
var utilidades = require('../controladores/utilidades');
//--------PERSONA----//
var personaControl = require('../controladores/PersonaControl');
var persona = new personaControl();
//--------CUENTA----//
var cuentaControl = require('../controladores/CuentaControlador');
var cuenta = new cuentaControl();

//funcion de autentificacion
var auth = function middleWare(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash("error", "Debes iniciar sesión primero!!");
        res.redirect('/inicio_sesion');
    }
};

/**
 * Funcion otorga permisos
 *  al administrador
 */
var admin = function middleWare(req, res, next) {
    if (req.user.rol === 'administrador') {
        next();
    } else {
        req.flash('error', 'No tiene permisos para agregar o modificar!');
        res.redirect('/');
    }
};

router.get('/', function (req, res, next) {
    if (req.isAuthenticated()) {
        var entrada = utilidades.verificar_rol(req);
        if (entrada) {
            res.render('index', {
                title: 'Administrador',
                fragmento: 'principal/frm_principal',
                user: req.user,
                rol: entrada
            });
        } else {
            res.render('index', {
                title: 'Encarta Nueva Generacion',
                fragmento: 'principal/frm_principal',
                user: req.user,
                rol: entrada
            });
        }

    } else {
        utilidades.crearRoles();
        res.render('index', {
            title: 'KIDOS',
            fragmento: 'principal/frm_principal',
            user: req.user,
            rol: entrada
        });
    }
});

//------------SESION-PERSONA-------//
router.get('/inicio_sesion', function (req, res) {
    res.render('index', {
        title: 'Inicio Sesion',
        fragmento: 'usuario/frm_sesion'
    });
});
router.post('/inicio_sesion',
        passport.authenticate('local-signin',
                {successRedirect: '/',
                    failureRedirect: '/inicio_sesion',
                    failureFlash: true}
        ));
router.get('/cerrar_sesion', auth, cuenta.cerrar_sesion);

//-----PERSONA-------//
router.get('/registro', persona.visualizar_registro);
router.post('/registro', persona.guardar);


module.exports = router;
