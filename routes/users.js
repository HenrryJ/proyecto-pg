var express = require('express');
var router = express.Router();

/* GET users listing. */

router.get('/sincronizar', function (req, res, next) {
//  res.send('respond with a resource');
    var models = require('./../models');
    models.sequelize.sync().then(() => {
        console.log('Sincronizado');
        res.send('Sincronizado');
    }).catch(err => {
        console.log("no Sincronizado");
        res.send("no Sincronizado");
    });
});

var auth = function (req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.rol === 'usuario') {
            next();
        } else {
            req.flash('error', "Acceso Denegado como Administrador");
            res.redirect("/");
        }

    } else {
        req.flash('error', "Debe iniciar sesion primero");
        res.redirect("/");
    }
};



router.get('/', auth, function (req, res, next) {
    res.render('index', {title: 'Sesion de Usuario'});
});

module.exports = router;
