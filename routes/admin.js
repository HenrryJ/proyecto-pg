var express = require('express');
var router = express.Router();
//--------UTILIDADES----//
var utilidades = require('../controladores/utilidades');
//--------PERSONA----//
var personaControl = require('../controladores/PersonaControl');
var persona = new personaControl();
//--------CUENTA----//
var cuentaControl = require('../controladores/CuentaControlador');
var cuenta = new cuentaControl();
//--------MATERIA----//
var materiaControl = require('../controladores/MateriaControl');
var materia = new materiaControl();
//--------NIVEL----//
var nivelControl = require('../controladores/NivelControl');
var nivel = new nivelControl();
//--------CONTENIDO----//
var contenidoControl = require('../controladores/ContenidoControl');
var contenido = new contenidoControl();
//--------MATERIAL----//
var materialControl = require('../controladores/Material_avControl');
var material = new materialControl();
//--------TEST----//
var testControl = require('../controladores/TestControl');
var test = new testControl();
//--------PREGUNTA----//
var preguntaControl = require('../controladores/PreguntaControl');
var pregunta = new preguntaControl();
//--------PROGRESO----//
var progresoControl = require('../controladores/ProgresoControl');
var progreso = new progresoControl();


//funcion de autentificacion
var auth = function middleWare(req, res, next) {
    if (req.isAuthenticated()) {
        utilidades.verificaEstadoSesion(req,res,next);



    } else {
        req.flash("error", "Debes iniciar sesión primero!!");
        res.redirect('/inicio_sesion');
    }
};

/**
 * Funcion Ototga permisos para modificar o registrar en la pagina
 */
var admin = function middleWare(req, res, next) {
    if (req.user.rol === 'administrador') {
        next();
    } else {
        req.flash('error', 'No tiene permisos para agregar o modificar!');
        res.redirect('/');
    }
};

//--------PERSONA----//
router.get('/verDatos', auth, persona.verPersona);
//--------GUARDAR-DATOS-PERSONA----//
router.post('/verDatos/guardar', auth, persona.modificarDatos);
router.post('/verDatos/guardar/cuenta', auth, persona.modificarCuenta);
//--------FOTO-PERSONA----//
router.post('/pefil/foto/guardar', auth, persona.foto_Perfil);
//--------LISTA-PERSONA----//
router.get('/lista/usuarios', auth, admin, persona.visualizar_usuarios);
router.get('/consultas/notas', auth, admin, persona.notas);
//--------DAR-BAJA-ALTA-PERSONA----//
router.get('/lista/usuarios/darBaja/:external', auth, admin, persona.darBajaUsuario);
router.get('/lista/usuarios/darAlta/:external', auth, admin, persona.darAltaUsuario);
//--------CURSOS----//
router.get('/lista/cursos', auth, materia.lista_materias);
//--------REGISTRO-CURSOS----//
router.get('/registro/cursos', auth, admin, materia.registro);
router.post('/registro/cursos', auth, admin, materia.guardar);
//--------MODIFICAR-CURSOS ----//
router.get('/modificar/cursos/:external', auth, admin, materia.visualizar_modificar);
router.post('/modificar', auth, admin, materia.modificar);
//--------DAR-BAJA-DAR-ALTA CURSOS----//
router.get('/darBaja/:external', auth, admin, materia.dar_Baja);
router.get('/darAlta/:external', auth, admin, materia.dar_Alta);
router.get('/lista/cursos_baja', auth, admin, materia.materia_inactivas);
//--------FOTO----//
router.get('/foto/:external', auth, admin, materia.visualizarR_Foto);
router.post('/foto/guardar', auth, admin, materia.subir_foto);
//--------NIVEL----//
router.get('/registro/nivel/:external', auth, admin, nivel.visualizar_registro);
router.post('/registro/nivel', auth, admin, nivel.guardar);
router.get('/nivel/modificar/:external', auth, admin, nivel.visualizar_modificar);
router.post('/modificar/nivel', auth, admin, nivel.modificar);
router.get('/nivel/baja/:external', auth, admin, nivel.dar_baja);
router.get('/nivel/alta/:external', auth, admin, nivel.dar_alta);
//--------LISTA-NIVEL----//
router.get('/lista/nivel/:external', auth, nivel.visualizar_niveles);
//---------CONTENIDO---------//
router.get('/contenido/:external', auth, contenido.visualizar);
router.post('/contenido/guardar', auth, admin, contenido.guardarContenido);
router.post('/contenido/modificar', auth, admin, contenido.modificar);
router.get('/contenido/presentar/:external', auth, contenido.listaTitulos);
router.get('/contenido/baja/:external', auth, admin, contenido.dar_baja);
router.get('/contenido/alta/:external', auth, admin, contenido.dar_alta);
router.get('/contenido/presentar/titulo/:external', auth, contenido.visualizarContenido);
//---------MATERIAL---------//
router.get('/material/registro/:external', auth, admin, material.visualizarRegistro);
router.post('/material/guardar', auth, admin, material.guardar);
router.get('/material/modificar/:external', auth, admin, material.visualizarModificar);
router.post('/material/modificar', auth, admin, material.modificar);
router.get('/material/darBaja/:external', auth, admin, material.darBaja);
router.get('/material/darAlta/:external', auth, admin, material.darAlta);
//---------TEST---------//
router.get('/test/registro/:external', auth, admin, test.visualizarRegistro);
router.post('/test/guardar', auth, admin, test.guardar);
router.get('/test/modificar/:external', auth, admin, test.visualizarModificar);
router.post('/test/modificar', auth, admin, test.modificar);
router.get('/test/darBaja/:external', auth, admin, test.darBaja);
router.get('/test/darAlta/:external', auth, admin, test.darAlta);
//---------PREGUNTA---------//
router.get('/pregunta/registro/:external', auth, admin, pregunta.visualizarRegistro);
router.post('/pregunta/guardar', auth, admin, pregunta.guardar);
router.get('/pregunta/:external', auth, pregunta.listarPreguntas);
router.get('/pregunta/darBaja/:external', auth, admin, pregunta.darBaja);
router.get('/pregunta/darAlta/:external', auth, admin, pregunta.darAlta);
//---------PROGRESO---------//
router.post('/progreso/guardar', auth, progreso.guardar);
module.exports = router;
