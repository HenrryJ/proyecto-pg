'use strict';
/**
 * Modelos de base de datos
 */
var models = require('./../models/');
var uuid = require('uuid');
var material = models.material_av;
/**
 * Permite trabajar con los metodos de este archivo
 */
var utilidades = require('../controladores/utilidades');

/**
 * Clase permite manipular los modelos en conjunto con las vista
 */
class Material_avControl {
    /**
     * Metodo redireccion a las vista de registro de material
     * Ejecuta la consulta de contenido por el external
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizarRegistro(req, res) {
        var external = req.params.external;
        models.contenido.findOne({where: {external_id: external, estado: true}, include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}).then(function (cont) {
            var verificar = utilidades.verificar_rol(req);
            if (cont) {
                res.render('index', {
                    title: 'Registro Material',
                    fragmento: 'material/frm_registro',
                    contenido: cont,
                    rol: verificar
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Guarda los datos del material 
     * Ejecuta la consulta con contenido para asignarle el id de contenido
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    guardar(req, res) {
        models.contenido.findOne({where: {external_id: req.body.contenido}, include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}).then(function (contenido) {
            if (contenido) {
                var dataM = {
                    id_contenido: contenido.id,
                    external_id: uuid.v4(),
                    titulo: req.body.tituloMaterial,
                    ruta: req.body.ruta,
                    descripcion: req.body.desc,
                    estado: true
                };
                material.create(dataM).then(function (ok) {
                    req.flash('success', 'Se registro el material_av! y el contenido');
                    res.redirect('/admin/contenido/presentar/' + contenido.nivel.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se registro el material_av ! solo el contenido');
                    res.redirect('/admin/lista/nivel/' + contenido.nivel.materia.external_id);
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'No se encontro el contenido');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Redirecciona a la vista de modificar 
     * Envia un objeto con los datos del material a modificar
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizarModificar(req, res) {
        var external = req.params.external;
        var verificar = utilidades.verificar_rol(req);
        material.findAll({where: {external_id: external}, include: [{model: models.contenido, as: 'contenido', include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}]}).then(function (materialE) {
            if (materialE.length > 0) {
                var material_av = materialE[0];
                res.render('index', {
                    title: 'Modificar Material',
                    fragmento: 'material/frm_modificar',
                    material: material_av,
                    rol: verificar
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Actualiza los datos del materia que eligio el usuario
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    modificar(req, res) {
        material.findOne({where: {external_id: req.body.material}, include: [{model: models.contenido, as: 'contenido', include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}]}).then(function (materia) {
            if (materia) {
                materia.titulo = req.body.tituloMaterial;
                materia.ruta = req.body.ruta;
                materia.descripcion = req.body.desc;
                materia.save().then(function (ok) {
                    req.flash('success', 'Se ha modificado');
                    res.redirect('/admin/contenido/presentar/titulo/' + materia.contenido.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se pudo modificar');
                    res.redirect('/admin/contenido/presentar/titulo/' + materia.contenido.external_id);
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function () {
            req.flash('error', 'Hubo un problema');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Metodo cambia el estado del materia a false  
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    darBaja(req, res) {
        var external = req.params.external;
        material.findOne({where: {external_id: external}, include: [{model: models.contenido, as: 'contenido', include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}]}).then(function (materia) {
            if (materia) {
                materia.estado = false;
                materia.save().then(function (ok) {
                    req.flash('success', 'Se ha dado de baja la imagen');
                    res.redirect('/admin/contenido/presentar/titulo/' + materia.contenido.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se pudo dar de baja');
                    res.redirect('/admin/contenido/presentar/titulo/' + materia.contenido.external_id);
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function () {
            req.flash('error', 'Hubo un problema');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Metodo cambia el estado del material a true
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    darAlta(req, res) {
        var external = req.params.external;
        material.findOne({where: {external_id: external}, include: [{model: models.contenido, as: 'contenido', include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}]}).then(function (materia) {
            if (materia) {
                materia.estado = true;
                materia.save().then(function (ok) {
                    req.flash('success', 'Se ha dado de alta la imagen');
                    res.redirect('/admin/contenido/presentar/titulo/' + materia.contenido.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se pudo dar de alta');
                    res.redirect('/admin/contenido/presentar/titulo/' + materia.contenido.external_id);
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function () {
            req.flash('error', 'Hubo un problema');
            res.redirect('/admin/lista/cursos');
        });
    }
}
module.exports = Material_avControl;

