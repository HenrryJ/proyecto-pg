"use strict";
/**
 * Modelos base de datos
 */
var models = require('./../models/');
var utilidades = require('../controladores/utilidades');
var uuid = require('uuid');
/**
 * Modulo de sequelize
 */
var sequelize = require('sequelize');
/**
 * Permite manipular los modelos y vistas
 */
class PreguntaControl {

    /**
     * Metodo redirecciona a la vista de registro de Preguntas
     * En ella se encuentra una lista para el administrador pueda ir visualizando las preguntas que ha registrado
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizarRegistro(req, res) {
        var external = req.params.external;
        var verificar = utilidades.verificar_rol(req);
        models.test.findOne({include: [{model: models.nivel, as: 'nivel', where: {external_id: external, estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}).then(function (test) {
            if (test) {
                models.pregunta.findAll({include: [{model: models.respuesta, as: 'respuestas'}, {model: models.test, as: 'test', where: {external_id: test.external_id}}]}).then(function (listaP) {
                    console.log(listaP);
                    res.render('index', {
                        title: 'Preguntas',
                        fragmento: 'pregunta/frm_tabla_preguntas',
                        test: test,
                        preguntas: listaP,
                        rol: verificar,
                        long:listaP.length
                    });
                }).error(function (error) {
                    req.flash('error', 'No se encuentra el test');
                    res.redirect('/');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error');
            res.redirect('/');
        });
    }

    /**
     * Metodo guarda los datos de la preguntas con sus respuestas
     * Usando el metodo bulkcreate guardamos las 4 respuestas de cada pregunta siendo una correcta
     * y las demas incorrectas, consulta el external_id del test para obtener el id del test al que se le
     * registraran las preguntas
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    guardar(req, res) {
        models.test.findOne({where: {external_id: req.body.test}, include: [{model: models.nivel, as: 'nivel', estado: true}]}).then(function (test) {
            if (test) {
                var dataP = {
                    external_id: uuid.v4(),
                    pregunta: req.body.pregunta,
                    estado: true,
                    id_test: test.id
                };
                models.pregunta.create(dataP).then(function (ok) {
                    models.respuesta.bulkCreate([
                        {id_pregunta: ok.id, external_id: uuid.v4(), valor: req.body.estado, respuesta: req.body.respuestaC, estado: true},
                        {id_pregunta: ok.id, external_id: uuid.v4(), valor: req.body.estadoI1, respuesta: req.body.respuestaI1, estado: true},
                        {id_pregunta: ok.id, external_id: uuid.v4(), valor: req.body.estadoI2, respuesta: req.body.respuestaI2, estado: true},
                        {id_pregunta: ok.id, external_id: uuid.v4(), valor: req.body.estadoI3, respuesta: req.body.respuestaI3, estado: true}
                    ]);
                    req.flash('success', 'Se ha registrado la pregunta!');
                    res.redirect('/admin/pregunta/registro/' + test.nivel.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se ha registrado la pregunta!');
                    res.redirect('/admin/contenido/presentar/' + test.nivel.external_id);
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error');
            res.redirect('/');
        });
    }

    /**
     * Metodo redirecciona a la lista de preguntas con sus respuesta 
     * Consulta el external_id del test para enviarlo al formulario 
     * Ejecuta una consulta en la cual nos presenta 10 preguntas en orden aleatorio
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    listarPreguntas(req, res) {
        var external = req.params.external;
        var verificar = utilidades.verificar_rol(req);
        models.test.findOne({include: [{model: models.nivel, as: 'nivel', where: {external_id: external, estado: true},include:[{model:models.materia,as:'materia',where:{estado:true} }]}]}).then(function (test) {
            if (test) {
                models.pregunta.findAll({order: [sequelize.fn('RAND')], limit: 10, include: [{model: models.respuesta, as: 'respuestas'},
                        {model: models.test, as: 'test', where: {external_id: test.external_id}}]}).then(function (lista) {
                    res.render('index', {
                        title: 'Preguntas',
                        fragmento: 'pregunta/frm_presentacion',
                        preguntas: lista,
                        test: test,
                        rol: verificar,
                        long: lista.length
                    });
                }).error(function (error) {
                    req.flash('error', 'Hubo un error');
                    res.redirect('/');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error');
            res.redirect('/');
        });
    }
    /**
     * Metodo modifica el estado de la pregunta cambiando a false
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    darBaja(req, res) {
        var external = req.params.external;
        models.pregunta.findOne({where: {external_id: external}, include: [{model: models.test, as: 'test', include: [{model: models.nivel, as: 'nivel', where: {estado: true}}]}]}).then(function (pregunta) {
            if (pregunta) {
                pregunta.estado = false;
                pregunta.save().then(function (ok) {
                    req.flash('success', 'Se ha dado de baja la pregunta!');
                    res.redirect('/admin/pregunta/registro/' + pregunta.test.nivel.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se modifico el estado!');
                    res.redirect('/admin/lista/cursos');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error');
            res.redirect('/admin/lista/cursos');
        });
    }
    /**
     * Metodo modifica el estado de la pregunta cambiandolo a true
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    darAlta(req, res) {
        var external = req.params.external;
        models.pregunta.findOne({where: {external_id: external}, include: [{model: models.test, as: 'test', include: [{model: models.nivel, as: 'nivel', where: {estado: true}}]}]}).then(function (pregunta) {
            if (pregunta) {
                pregunta.estado = true;
                pregunta.save().then(function (ok) {
                    req.flash('success', 'Se ha dado de alta la pregunta!');
                    res.redirect('/admin/pregunta/registro/' + pregunta.test.nivel.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se modifico el estado!');
                    res.redirect('/admin/lista/cursos');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error');
            res.redirect('/admin/lista/cursos');
        });
    }
}

module.exports = PreguntaControl;