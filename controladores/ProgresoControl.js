'use strict';
/**
 * Modelos de la base de datos
 */
var models = require('./../models/');
var uuid = require('uuid');
/**
 * Permite manipular los modelos y vistas
 */
class ProgresoControl {

    /**
     * Metodo guarda el progreso del test con la persona en la tabla progreso 
     * Consultando con el parametro external el id del test
     * Consulta con la variable de sesion el id del usuario que esta realizando el test
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    guardar(req, res) {
        models.test.findOne({where: {external_id: req.body.test}, include: [{model: models.nivel, as: 'nivel', where: {estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}]}).then(function (test) {
            if (test) {
                console.log(test.nombre + '  =====  id = ' + test.id);
                models.persona.findOne({where: {external_id: req.user.id}}).then(function (persona) {
                    if (persona) {
                        console.log('Persona = == = = = == = = = = ' + persona.nombres + '  id = ' + persona.id);
                        console.log('Re.user.id ==================================================================='+req.user.id);
                        models.progreso.findOne({where:{id_persona: persona.id,id_test: test.id}}).then(function (existe) {
                            if (existe) {
                                existe.calificacion = req.body.nota + '';
                                existe.save().then(function (update) {
                                    if (update) {
                                        req.flash('success', 'Se modifico su resultado anterior del test!');
                                        res.redirect('/admin/contenido/presentar/' + test.nivel.external_id);
                                    } else {
                                        req.flash('error', 'No se registrar la nueva nota del test');
                                        res.redirect('/admin/contenido/presentar/' + test.nivel.external_id);
                                    }

                                });
                            } else {
                                var dataP = {
                                    external_id: uuid.v4(),
                                    calificacion: req.body.nota + '',
                                    id_test: test.id,
                                    id_persona: persona.id
                                };
                                models.progreso.create(dataP).then(function (save) {
                                    req.flash('success', 'Se registro el resultado del test!');
                                    res.redirect('/admin/contenido/presentar/' + test.nivel.external_id);
                                });
                            }
                        });
                    } else {
                        req.flash('error', 'No se encontro el usuario!');
                        res.redirect('/admin/contenido/presentar/' + test.nivel.external_id);
                    }
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        });
    }
}
module.exports = ProgresoControl;

