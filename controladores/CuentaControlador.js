'use strict';
/**
 * Clase permite manejar la session
 */
class CuentaControlador {
    /**
     * Metodo cierra la sesion del usuario 
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    cerrar_sesion(req, res) {
        req.session.destroy();
        res.redirect("/");
    }
}
module.exports = CuentaControlador;
