'use strict';
/**
 * Modelos de la base de datos
 */
var models = require('./../models/');
var nivel = models.nivel;
var materia = models.materia;
var uuid = require('uuid');
/**
 * Metodos
 */
var utilidades = require('../controladores/utilidades');

/**
 * Permite manipular los modelos y vistas
 */
class NivelControl {
    /**
     * Metodo redireciona a la ventana de niveles
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizar_niveles(req, res) {
        var external = req.params.external;
        materia.findAll({where: {external_id: external, estado: true}}).then(function (mat) {
            var verificar = utilidades.verificar_rol(req);
            if (mat.length > 0) {
                var materias = mat[0];
                nivel.findAll({include: [{model: models.materia, as: 'materia', where: {external_id: external}}, {model: models.test, as: 'test'}]}).then(function (niveles) {
                    res.render('index', {
                        title: 'Niveles',
                        fragmento: 'nivel/frm_niveles',
                        mat: materias,
                        user: req.user,
                        rol: verificar,
                        niveles
                    });
                }).error(function (error) {
                    req.flash('error', 'Hubo un error!');
                    res.redirect('/admin/lista/cursos');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error !');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Metodo redirecciona a la ventana de reguistro de nivel
     * en ella se hace una consulta para poder enviar el external de materia
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizar_registro(req, res) {
        var external = req.params.external;
        var verificar = utilidades.verificar_rol(req);
        materia.findAll({where: {external_id: external, estado: true}}).then(function (dato) {
            if (dato.length > 0) {
                var mat = dato[0];
                nivel.findAll({include: [{model: models.materia, as: 'materia', where: {external_id: external}}]}).then(function (lista) {
                    var nro = (lista.length + 1);
                    res.render('index', {
                        title: 'Registro Nivel',
                        fragmento: 'nivel/frm_registro',
                        user: req.user,
                        rol: verificar,
                        nro: nro,
                        mat: mat
                    });
                }).error(function (error) {
                    req.flash('error', 'Hubo un error!');
                    res.redirect('/admin/lista/cursos');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect('/admin/lista/cursos');
        });
    }

    /**
     * Metodo guarda el nivel con su materia 
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    guardar(req, res) {
        console.log("Ext: " + req.body.materia);
        materia.findAll({where: {external_id: req.body.materia}}).then(function (mat) {
            if (mat.length > 0) {
                var materias = mat[0];
                var dataN = {
                    external_id: uuid.v4(),
                    nombre: req.body.nombre,
                    nro: req.body.nro,
                    descripcion: req.body.descripcion,
                    estado: true,
                    id_materia: materias.id
                };
                nivel.create(dataN).then(function (save) {
                    req.flash('success', 'Se registro el nivel!');
                    res.redirect('/admin/lista/nivel/' + materias.external_id);
                }).error(function (error) {
                    req.flash('error', 'No se registro el nivel!');
                    res.redirect('/admin/lista/cursos');
                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect('/admin/lista/cursos');
        });
    }
    /**
     * Con este metodo visualizamos el nivel a modificar
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizar_modificar(req, res) {
        nivel.findOne({where: {external_id: req.params.external, estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}).then(function (data) {
            if (data) {
                var verificar = utilidades.verificar_rol(req);
                res.render('index', {
                    title: 'Modificar Nivel ' + data.nro,
                    fragmento: 'nivel/frm_modificar',
                    user: req.user,
                    rol: verificar,
                    niv: data
                });

            } else {
                req.flash('error', 'La materia o el nivel fueron dados de baja');
                res.redirect('/admin/lista/cursos');
            }
        });
    }
    /**
     * Con este metodo modificamos el nivel previamente visualizado
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    modificar(req, res) {
        nivel.findOne({where: {external_id: req.body.nivel, estado: true}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}).then(function (data) {
            if (data) {
                data.nombre = req.body.nombre;
                data.descripcion = req.body.descripcion;
                data.save().then(function (dat) {
                    if (dat) {
                        req.flash('success', 'Se  modifico el nivel  Nro ' + data.nro + ' de ' + data.materia.nombre);
                        res.redirect('/admin/lista/nivel/' + data.materia.external_id);
                    } else {
                        req.flash('error', 'No se pudo modificar');
                        res.redirect('/admin/lista/cursos');
                    }
                });

            } else {
                req.flash('error', 'La materia o el nivel fueron dados de baja');
                res.redirect('/admin/lista/cursos');
            }
        });

    }
    /**
     * Este metodo permite dar de baja un nivel
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    dar_baja(req, res) {
        var external = req.params.external;
        nivel.findOne({where: {external_id: external}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}).then(function (dataN) {
            if (dataN) {
                dataN.estado = false;
                dataN.save().then(function (down) {
                    if (down) {
                        req.flash('success', 'Nivel se ha dado de baja');
                        res.redirect('/admin/lista/nivel/' + dataN.materia.external_id);
                    } else {
                        req.flash('error', 'No se ha podido dar de baja');
                        res.redirect('/admin/lista/nivel/' + dataN.materia.external_id);
                    }

                });
            } else {
                req.flash('error', 'La materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        });
    }
    /**
     * Este metodo permite dar de alta un nivel
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    dar_alta(req, res) {
        var external = req.params.external;
        nivel.findOne({where: {external_id: external}, include: [{model: models.materia, as: 'materia', where: {estado: true}}]}).then(function (dataN) {
            if (dataN) {
                dataN.estado = true;
                dataN.save().then(function (down) {
                    if (down) {
                        req.flash('success', 'Nivel se ha dado de alta');
                        res.redirect('/admin/lista/nivel/' + dataN.materia.external_id);
                    } else {
                        req.flash('error', 'No se ha podido dar de alta');
                        res.redirect('/admin/lista/nivel/' + dataN.materia.external_id);
                    }

                });
            } else {
                req.flash('error', 'Hubo un problema el nivel o la materia fue dada de baja!');
                res.redirect('/admin/lista/cursos');
            }
        });
    }
}
module.exports = NivelControl;
