'use strict';
/**
 * Modelos de la base de datos
 */
var models = require('./../models/');
var uuid = require('uuid');
/**
 * Modulo de encriptacion de claves
 */
var bcrypt = require('bcrypt');
/**
 * Costo
 */
const saltRounds = 8;

/**
 * Esta funcion crea dos roles 
 * junto con una persona y su respectiva cuenta en el 
 * caso que los roles no existan caso contrario el metodo no creara hara nada
 * @returns {undefined} 
 */
function crearRoles() {
    var rol = models.rol;
    rol.findAll().then(function (lista) {
        if (lista.length === 0) {
            rol.bulkCreate([
                {nombre: 'administrador'},
                {nombre: 'usuario'}
            ]).then(() => {
                return rol.findOne({where: {nombre: 'administrador'}});
            }).then(ROL => {
                var persona = models.persona;
                var encriptador = function (clave) {
                    return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);
                };
                var data = {
                    apellidos: 'administardor',
                    external_id: uuid.v4(),
                    nombres: 'administardor',
                    nickname: 'admin',
                    fecha_nac: '1999-12-31',
                    edad: 20,
                    id_rol: ROL.id,
                    cuenta: {
                        correo: 'proyecto@gmail.com',
                        external_id: uuid.v4(),
                        clave: encriptador('1234'),
                        estado: true
                    }
                };
                persona.create(data, {include: [{model: models.cuenta, as: 'cuenta'}]});
            });
        }
    });
}

/**
 * Devuelve un booleano segun el tipo de usuario logeado
 * @param {type} req recibe el user.rol verifica si es el administrador el que esta iniciado sesion
 * @returns {Boolean}
 */
function verificar_rol(req) {
    if (req.user.rol === "administrador") {
        return true;
    } else {
        return false;
    }
}
/**
 * Este metodo te permite verificar si un usuario fue dado de baja
 * mientras estaba en la sesion, en el caso que este dado de baja, 
 * automaticamente se le cerrara la sesion 
 * @param {type} req
 * @param {type} res
 * @param {type} next
 * @returns {undefined}
 */
function verificaEstadoSesion(req,res,next) {
    models.cuenta.findOne({where: {estado: true}, include: [{model: models.persona, where: {external_id: req.user.id}}]}).then(function (activo) {
        if (activo) {
            next();
        } else {
            req.flash("error", "Tu cuenta fue dada de baja");
            res.redirect('/cerrar_sesion');
        }
    });
}


module.exports = {crearRoles, verificar_rol, verificaEstadoSesion};

