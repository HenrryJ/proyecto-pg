'use strict';
/**
 * Modelos de la base de datos
 */
var models = require('./../models/');
var materia = models.materia;
var uuid = require('uuid');
var utilidades = require('../controladores/utilidades');
//Subir Fto
/**
 * Require formidable
 */
var formidable = require('formidable');
/**
 * Extenciones permitidas para subir la imagen
 */
var extensiones = ["jpg", "png", "gif"];
/**
 * Tamaño maximo de la imagen 
 * @type Number
 */
var maxSize = 1 * 1024 * 1024;
/**
 * fs de fromidable
 */
var fs = require('fs');
/*
 * Dependencia que permite mover archivos
 */
var mv = require('mv');
//END subir foto

/**
 * Clase permite la manipulacion de los modelos y vistas
 */
class MateriaControl {
    /**
     * Envia una lista de todas las materias
     * rol: verifica que usuario esta inicia sesion y muestra diferentes ventanas segun el usuario!
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    lista_materias(req, res) {
        materia.findAll({where:{estado:true}}).then(function (materias) {
            var verificar = utilidades.verificar_rol(req);
            res.render('index', {
                title: 'Listado Materias',
                fragmento: 'cursos/frm_cursos',
                materias: materias,
                rol: verificar
            });
        }).error(function (error) {
            res.redirect("/");
        });
    }

    /**
     * Presenta la vista de registro
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    registro(req, res) {
        var verificar = utilidades.verificar_rol(req);
        res.render('index', {
            title: 'Registro Materia',
            fragmento: 'cursos/frm_registro',
            rol: verificar
        });
    }
    
    /**
     * Guarda la materia enviando la data al 
     * modelo de la BD
     * @param {type} req
     * @param {type} res
     * @returns {undefined}F
     */
    guardar(req, res) {
        var dataM = {
            external_id: uuid.v4(),
            nombre: req.body.nombre,
            codigo: req.body.codigo,
            detalle: req.body.detalle,
            estado: true
        };
        materia.create(dataM).then(function (save) {
            req.flash('success', 'Se ha Guardado la Materia ' + dataM.nombre + ' con Exito!');
            res.redirect("/admin/lista/cursos");
        }).error(function (error) {
            req.flash('error', 'No se pudo guarda la Materia!');
            res.redirect("/admin/lista/cursos");
        });
    }
    /**
     * Metodo redirecciona al metodo a la vista de registro de foto
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizarR_Foto(req, res) {
        var external = req.params.external;
        materia.findAll({where: {external_id: external}}).then(function (materias) {
            var verificar = utilidades.verificar_rol(req);
            if (materias.length > 0) {
                var mat = materias[0];
                res.render('index', {
                    title: 'Registro Foto',
                    fragmento: 'cursos/frm_foto_materia',
                    mat: mat,
                    rol: verificar
                });
            }
        }).error(function (error) {
            req.flash('error', 'No se encontro la materia');
            res.redirect("/admin/lista/cursos");
        });
    }

    /**
     * Metodo permite guardar la imagen en la carpeta uploads 
     * Guarda el nombre de la imagen en la base de datos
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    subir_foto(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (files.archivo.size <= maxSize) {
                console.log(files.archivo);
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = fields.external;
                    var nombreFoto = uuid.v4() + "." + extension;
                    mv(files.archivo.path, "public/uploads/" + nombreFoto,
                            function (err) {
                                if (err) {
                                    req.flash('error', "Hubo un error " + err);
                                    res.redirect("/admin/lista/cursos");
                                } else {
                                    materia.findAll({where: {external_id: external}}).then(function (resultado) {
                                        if (resultado.length > 0) {
                                            console.log("Se Encontro Materia")
                                            var materiaC = resultado[0];
                                            materiaC.foto = nombreFoto;
                                            materiaC.save().then(function (save) {
                                                console.log("Guardado");
                                                fs.exists(files.archivo.path, function (exists) {
                                                    if (exists)
                                                        fs.unlinkSync(files.archivo.path);
                                                });
                                                req.flash('success', "Se ha cambiado su foto de perfil");
                                                res.redirect("/admin/lista/cursos");
                                            });
                                        }
                                    });
                                }
                            });
                } else {
                    console.log("ARCHIVO NO VALIDO");
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o gif");
                    res.redirect("/admin/lista/cursos");
                }
            } else {
                console.log("TAMAÑO SUPERA 1M");
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/admin/lista/cursos");
            }
        });
    }

    /**
     * Presenta la vista visualizar modificar
     * Envia los datos de la materia al fragmento
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizar_modificar(req, res) {
        var external = req.params.external;
        materia.findAll({where: {external_id: external}}).then(function (materiaE) {
            var verificar = utilidades.verificar_rol(req);
            if (materiaE.length > 0) {
                var mat = materiaE[0];
                res.render('index', {
                    title: 'Modificar',
                    fragmento: 'cursos/frm_modificar',
                    mat: mat,
                    rol: verificar
                });
            } else {
                req.flash('error', 'No existe!');
                res.redirect("/admin/lista/cursos");
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect("/admin/lista/cursos");
        });
    }

    /**
     * Modifica la materia consultando el external_id
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    modificar(req, res) {
        materia.findAll({where: {external_id: req.body.external}}).then(function (data) {
            if (data.length > 0) {
                var mat = data[0];
                mat.nombre = req.body.nombre;
                mat.codigo = req.body.codigo;
                mat.detalle = req.body.detalle;
                mat.save().then(function (pro) {
                    if (mat.estado === true) {
                        req.flash('success', 'Se ha Modificado la Materia ' + mat.nombre + ' con Exito');
                        res.redirect("/admin/lista/cursos");
                    } else {
                        req.flash('success', 'Se ha Modificado la Materia ' + mat.nombre + ' con Exito');
                        res.redirect("/admin/lista/cursos_baja");
                    }
                });
            } else {
                req.flash('error', 'No se puedo Encontrar!');
                res.redirect("/admin/lista/cursos");
            }
        }).error(function (error) {
            req.flash('error', 'No se puedo Modificar!');
            res.redirect("/admin/lista/cursos");
        });
    }

    /**
     * Método cambia el estado de la materia
     * Consultando con el external_id la materia
     * Y cambiando su estado a FALSE
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    dar_Baja(req, res) {
        var external = req.params.external;
        materia.findAll({where: {external_id: external}}).then(function (data) {
            if (data.length > 0) {
                var mat = data[0];
                mat.estado = false;
                mat.save().then(function (pro) {
                    req.flash('success', mat.nombre + ', Se ha dado de Baja!');
                    res.redirect("/admin/lista/cursos");
                });
            } else {
                req.flash('error', 'No se pudo dar de baja la Materia!');
                res.redirect("/admin/lista/cursos");
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect("/admin/lista/cursos");
        });
    }

    /**
     * Cambia es estado de la materia a true
     * Busca con el external_id
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    dar_Alta(req, res) {
        var external = req.params.external;
        materia.findAll({where: {external_id: external}}).then(function (data) {
            if (data.length > 0) {
                var mat = data[0];
                mat.estado = true;
                mat.save().then(function (pro) {
                    req.flash('success', mat.nombre + ', Se ha dado de Alta!');
                    res.redirect("/admin/lista/cursos_baja");
                });
            } else {
                req.flash('error', 'No se pudo dar de alta la Materia!');
                res.redirect("/admin/lista/cursos");
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect("/admin/lista/cursos");
        });
    }
    
    /**
     * Metodo presenta el listado de las materias dadas de baja
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    materia_inactivas(req, res) {
        materia.findAll({where:{estado:false}}).then(function (materias) {
            var verificar = utilidades.verificar_rol(req);
            res.render('index', {
                title: 'Listado Materias',
                fragmento: 'cursos/frm_cursos_baja',
                materias: materias,
                rol: verificar
            });
        }).error(function (error) {
            res.redirect("/");
        });
    }

}
module.exports = MateriaControl;

