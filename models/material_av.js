'use strict';
module.exports = (sequelize, DataTypes) => {
    const material_av = sequelize.define('material_av', {
        external_id: DataTypes.UUID(36),
        titulo: DataTypes.STRING(150),
        ruta: DataTypes.STRING(150),
        descripcion: DataTypes.STRING(255),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    material_av.associate = function (models) {
        // associations can be defined here
        material_av.belongsTo(models.contenido, {foreignKey: 'id_contenido'});
    };
    return material_av;
};