'use strict';
module.exports = (sequelize, DataTypes) => {
    const cuenta = sequelize.define('cuenta', {
        external_id: DataTypes.UUID(36),
        correo: DataTypes.STRING(30),
        clave: DataTypes.STRING(125),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    cuenta.associate = function (models) {
        // associations can be defined here
        cuenta.belongsTo(models.persona, {foreignKey: 'id_persona'});
    };
    return cuenta;
};