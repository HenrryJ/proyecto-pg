'use strict';
module.exports = (sequelize, DataTypes) => {
    const respuesta = sequelize.define('respuesta', {
        external_id: DataTypes.UUID(36),
        respuesta: DataTypes.STRING(255),
        valor: DataTypes.STRING(2),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    respuesta.associate = function (models) {
        // associations can be defined here
        respuesta.belongsTo(models.pregunta, {foreignKey: 'id_pregunta'});
    };
    return respuesta;
};