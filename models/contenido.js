'use strict';
module.exports = (sequelize, DataTypes) => {
    const contenido = sequelize.define('contenido', {
        external_id: DataTypes.UUID(36),
        titulo: DataTypes.STRING(150),
        descripcion: DataTypes.TEXT,
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    contenido.associate = function (models) {
        // associations can be defined here
        contenido.hasMany(models.material_av, {foreignKey: 'id_contenido', as: 'material_av'});
        contenido.belongsTo(models.nivel, {foreignKey: 'id_nivel'});
    };
    return contenido;
};