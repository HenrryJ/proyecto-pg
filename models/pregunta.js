'use strict';
module.exports = (sequelize, DataTypes) => {
    const pregunta = sequelize.define('pregunta', {
        external_id: DataTypes.UUID(36),
        pregunta: DataTypes.STRING(255),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    pregunta.associate = function (models) {
        // associations can be defined here
        pregunta.hasMany(models.respuesta, {foreignKey: 'id_pregunta', as: 'respuestas'});
        pregunta.belongsTo(models.test, {foreignKey: 'id_test'});
    };
    return pregunta;
};