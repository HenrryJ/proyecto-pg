'use strict';
module.exports = (sequelize, DataTypes) => {
    const test = sequelize.define('test', {
        external_id: DataTypes.UUID(36),
        nombre: DataTypes.STRING(150),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    test.associate = function (models) {
        // associations can be defined here
        test.belongsTo(models.nivel, {foreignKey: 'id_nivel'});
        test.hasMany(models.pregunta, {foreignKey: 'id_test', as: 'preguntas'});
        test.hasMany(models.progreso, {foreignKey: 'id_test', as: 'progreso'});
    };
    return test;
};

