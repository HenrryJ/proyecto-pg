'use strict';
module.exports = (sequelize, DataTypes) => {
    const materia = sequelize.define('materia', {
        external_id: DataTypes.UUID(36),
        nombre: DataTypes.STRING(30),
        codigo: DataTypes.STRING(30),
        detalle: DataTypes.STRING(255),
        foto: DataTypes.STRING(255),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    materia.associate = function (models) {
        // associations can be defined here
        materia.hasMany(models.nivel, {foreignKey: 'id_materia', as: 'materia'});
    };
    return materia;
};