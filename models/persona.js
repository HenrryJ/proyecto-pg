'use strict';
module.exports = (sequelize, DataTypes) => {
    const persona = sequelize.define('persona', {
        external_id: DataTypes.UUID(36),
        apellidos: DataTypes.STRING(100),
        nombres: DataTypes.STRING(100),
        nickname: DataTypes.STRING(75),
        fecha_nac: DataTypes.DATEONLY,
        edad: DataTypes.INTEGER(4), 
        foto: DataTypes.STRING(255)
    }, {freezeTableName: true});
    persona.associate = function (models) {
        // associations can be defined here
        persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
        persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
        persona.hasMany(models.progreso, {foreignKey: 'id_persona', as: 'progreso'});
    };
    return persona;
};