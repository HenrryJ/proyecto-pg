'use strict';
module.exports = (sequelize, DataTypes) => {
    const nivel = sequelize.define('nivel', {
        external_id: DataTypes.UUID(36),
        nombre: DataTypes.STRING(30),
        nro: DataTypes.INTEGER(2),
        descripcion: DataTypes.STRING(255),
        estado: DataTypes.BOOLEAN
    }, {freezeTableName: true});
    nivel.associate = function (models) {
        // associations can be defined here
        nivel.belongsTo(models.materia, {foreignKey: 'id_materia', as:'materia'});
        nivel.hasOne(models.test, {foreignKey: 'id_nivel', as: 'test'});
        nivel.hasMany(models.contenido, {foreignKey: 'id_nivel', as: 'contenido'});
    };
    return nivel;
};