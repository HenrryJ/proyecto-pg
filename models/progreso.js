'use strict';
module.exports = (sequelize, DataTypes) => {
    const progreso = sequelize.define('progreso', {
        external_id: DataTypes.UUID(36),
        calificacion: DataTypes.STRING(255)
    }, {freezeTableName: true});
    progreso.associate = function (models) {
        // associations can be defined here
        progreso.belongsTo(models.persona, {foreignKey: 'id_persona'});
        progreso.belongsTo(models.test, {foreignKey: 'id_test'});
    };
    return progreso;
};